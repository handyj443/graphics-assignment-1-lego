// Does basic ambient, diffuse, and specular lighting.
//=============================================================================


uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInverseTranspose;
uniform extern float4x4 gWVP;
uniform extern float4x4 gViewProj;

uniform extern float3 gLightVecW;
uniform extern float3 gEyePosW;

uniform extern float4 gDiffuseMtrl;
uniform extern float4 gDiffuseLight;
uniform extern float4 gAmbientMtrl;
uniform extern float4 gAmbientLight;
uniform extern float4 gSpecularMtrl;
uniform extern float4 gSpecularLight;
uniform extern float gSpecularPower;
uniform extern float gBrickSpecularPower;

uniform extern texture gGroundTex;
uniform extern texture gTreeTex;

uniform extern float gTime;

sampler GroundTexS = sampler_state
{
	Texture = <gGroundTex>;
	MinFilter = Anisotropic;
	MagFilter = Anisotropic;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU = WRAP;
	AddressV = WRAP;
};

sampler TreeTexS = sampler_state
{
	Texture = <gTreeTex>;
	MinFilter = Anisotropic;
	MagFilter = Anisotropic;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
};

struct OutputVS
{
	float4 posH : POSITION0;
	float3 posW : TEXCOORD0;
	float3 normalW : TEXCOORD1;
	float4 diffuse : COLOR0;
	float2 tex0 : TEXCOORD2;
};

OutputVS DirLightTexVS(float3 posL : POSITION0, float3 normalL : NORMAL0,
					   float2 tex0 : TEXCOORD0) {
	OutputVS outVS = (OutputVS)0;

	// Transform vertex to clip space and world space
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	outVS.posW = mul(float4(posL, 1.0f), gWorld).xyz;

	// Transform normal to world space
	float3 normalW = mul(float4(normalL, 0.0f), gWorldInverseTranspose).xyz;
	outVS.normalW = normalW;

	//_____________________________________________________________
	// Computer the color
	// Ambient
	float4 ambient = gAmbientMtrl * gAmbientLight;
	// Diffuse
	float4 diffuse = gDiffuseMtrl * gDiffuseLight * 
		max(dot(normalW, gLightVecW), 0.0f);
	outVS.diffuse.rgb = (ambient + diffuse).xyz;
	outVS.diffuse.a = gDiffuseMtrl.a;

	// Pass texture
	outVS.tex0 = tex0;

	return outVS;
}

float4 DirLightTexPS(float3 posW : TEXCOORD0, float3 normalW : TEXCOORD1,
					 float4 diffuse : COLOR0, float2 tex0 : TEXCOORD2) : COLOR0
{
	// Multiply by texture
	float3 texColor = tex2D(GroundTexS, tex0).rgb;
	float3 baseColor = texColor * diffuse.rgb;
	//float3 baseColor = diffuse.rgb;

	// Specular
	normalW = normalize(normalW);
	float3 vertexToEye = normalize(gEyePosW - posW);
	float3 halfAngle = normalize(vertexToEye + gLightVecW);
	float3 specular = (gSpecularMtrl * gSpecularLight *
					   pow(max(dot(normalW, halfAngle), 0), gSpecularPower)).rgb;
	float3 finalColor = baseColor + specular;

	return float4(finalColor, diffuse.a);
}

technique DirLightTexTech
{
	pass P0
	{
		vertexShader = compile vs_2_0 DirLightTexVS();
		pixelShader = compile ps_2_0 DirLightTexPS();
	}
}

// TREES
OutputVS TreeVS(float3 posL : POSITION0, float3 offset : TEXCOORD0,
				float2 tex0 : TEXCOORD1) {
	OutputVS outVS = (OutputVS)0;

	float3 look = gEyePosW - offset;
	look.y = 0.0f;  // keep look in xz plane
	look = normalize(look);
	float3 up = float3(0, 1, 0);
	float3 right = cross(up, look);

	float3x3 R;
	R[0] = right;
	R[1] = up;
	R[2] = look;

	outVS.posW = mul(posL, R) + offset;

	// Transform vertex to clip space and world space
	outVS.posH = mul(float4(outVS.posW, 1.0f), gViewProj);

	// Pass texture
	outVS.tex0 = tex0;

	return outVS;
}

float4 TreePS(float2 tex0 : TEXCOORD2) : COLOR0
{
	float4 color = tex2D(TreeTexS, tex0);
	return color;
}

technique TreeTech
{
	pass P0
	{
		vertexShader = compile vs_2_0 TreeVS();
		pixelShader = compile ps_2_0 TreePS();
		AlphaTestEnable = true;
		AlphaFunc = GreaterEqual;
		AlphaRef = 80;
		CullMode = None;
	}
}

// BRICKS
OutputVS LegoBrickVS(float3 posL : POSITION0, float3 normalL : NORMAL0,
				float4 col : COLOR0) {
	OutputVS outVS = (OutputVS)0;

	// Transform vertex to clip space and world space
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	outVS.posW = mul(float4(posL, 1.0f), gWorld).xyz;

	// Transform normal to world space
	outVS.normalW = mul(float4(normalL, 1.0f), gWorldInverseTranspose).xyz;

	//_____________________________________________________________
	// Compute the color
	// Ambient
	float4 ambient = col * gAmbientLight;
	// Diffuse
	float4 diffuse = col * gDiffuseLight * 
		max(dot(outVS.normalW, gLightVecW), 0.0f);
	outVS.diffuse.rgb = (ambient + diffuse).xyz;
	//outVS.diffuse.rgb = diffuse.xyz;
	outVS.diffuse.a = col.a;

	return outVS;
}

float4 LegoBrickPS(float3 posW : TEXCOORD0, float3 normalW : TEXCOORD1,
				   float4 diffuse : COLOR0) : COLOR0
{
	// Specular
	normalW = normalize(normalW);
	float3 vertexToEye = normalize(gEyePosW - posW);
	float3 halfAngle = normalize(vertexToEye + gLightVecW);
	float3 specular = (gSpecularMtrl * gSpecularLight *
					   pow(max(dot(normalW, halfAngle), 0),
						   gBrickSpecularPower)).rgb;
	float3 finalColor = diffuse.rgb + specular;

	return float4(finalColor, diffuse.a);
}

technique BrickTech
{
	pass P0
	{
		vertexShader = compile vs_2_0 LegoBrickVS();
		pixelShader = compile ps_2_0 LegoBrickPS();
		CullMode = None;
	}
}