#pragma once
#include <string>

const D3DCOLOR BLACK = D3DCOLOR_XRGB(27, 42, 52);
const D3DCOLOR WHITE = D3DCOLOR_XRGB(242, 243, 242);
const D3DCOLOR GREY = D3DCOLOR_XRGB(161, 165, 162);
const D3DCOLOR DARK_GREY = D3DCOLOR_XRGB(109, 110, 108);
const D3DCOLOR BRIGHT_RED = D3DCOLOR_XRGB(196, 40, 27);
const D3DCOLOR BRIGHT_YELLOW = D3DCOLOR_XRGB(245, 205, 47);
const D3DCOLOR BRIGHT_BLUE = D3DCOLOR_XRGB(13, 105, 171);
const D3DCOLOR DARK_GREEN = D3DCOLOR_XRGB(40, 127, 70);
const D3DCOLOR DARK_ORANGE = D3DCOLOR_XRGB(160, 95, 52);
const D3DCOLOR EARTH_ORANGE = D3DCOLOR_XRGB(98, 71, 50);
const D3DCOLOR BRIGHT_ORANGE = D3DCOLOR_XRGB(218, 133, 64);
const D3DCOLOR FLAME_REDDISH_ORANGE = D3DCOLOR_XRGB(207, 96, 36);

D3DCOLOR InterpretColor(std::string col) {
	if (col == "BLACK") {
		return BLACK;
	}
	if (col == "WHITE") {
		return WHITE;
	}
	if (col == "GREY") {
		return GREY;
	}
	if (col == "DARK_GREY") {
		return DARK_GREY;
	}
	if (col == "BRIGHT_RED") {
		return BRIGHT_RED;
	}
	if (col == "BRIGHT_YELLOW") {
		return BRIGHT_YELLOW;
	}
	if (col == "BRIGHT_BLUE") {
		return BRIGHT_BLUE;
	}
	if (col == "DARK_GREEN") {
		return DARK_GREEN;
	}
	if (col == "DARK_ORANGE") {
		return DARK_ORANGE;
	}
	if (col == "EARTH_ORANGE") {
		return EARTH_ORANGE;
	}
	if (col == "BRIGHT_ORANGE") {
		return BRIGHT_ORANGE;
	}
	if (col == "FLAME_REDDISH_ORANGE") {
		return FLAME_REDDISH_ORANGE;
	}
	return BLACK;  // default
}