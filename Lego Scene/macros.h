#pragma once

#include <tchar.h>
#include <cassert>

#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }

#if defined(DEBUG) | defined(_DEBUG)
#ifndef HR
#define HR(x)                                      \
		{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
			{                                              \
                MessageBox(0, _T("DirectX failed"), 0, 0); \
			}                                              \
		}
#endif

#else
#ifndef HR
#define HR(x) x;
#endif
#endif