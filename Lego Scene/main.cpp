// Displays a lego scene from a list of Lego Scene Description files
// World coordinates are in mm
// DirectX conventions: CW triangle winding, LH coordinate system

#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits>
#include <cmath>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include "InputControllers.h"
#include "BrickColors.h"
#include "GfxStats.h"

using std::string;
using std::vector;
using std::ifstream;
using std::sort;
#if defined(DEBUG) | defined(_DEBUG)
#define D3D_DEBUG_INFO
#endif
//#define TEXTURED_GROUND
//#define TEXTURED_TREES
#define MULTISAMPLE
#define STUD_OPTIMISATION

//______________________________________________________________________________
// MACROS
#include "macros.h"

//______________________________________________________________________________
// FORWARD DECLARATIONS
void enableFullScreenMode(bool enable);
void onResetDevice();
void onLostDevice();

//______________________________________________________________________________
// VERTEX DEFINITIONS
struct VertexPosNormalCol
{
	VertexPosNormalCol(float x, float y, float z, float nx, float ny, float nz,
		D3DCOLOR c)
		: pos(x, y, z), normal(nx, ny, nz), col(c) {}
	VertexPosNormalCol(float x, float y, float z, const D3DXVECTOR3 &normal,
		D3DCOLOR c)
		: pos(x, y, z), normal(normal), col(c) {}
	VertexPosNormalCol(const D3DXVECTOR3 &pos, const D3DXVECTOR3 &normal,
		D3DCOLOR c)
		: pos(pos), normal(normal), col(c) {}
	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DCOLOR col;
};
struct VertexPosNormalUV
{
	VertexPosNormalUV(float x, float y, float z, float nx, float ny, float nz,
		float u, float v)
		: pos(x, y, z), normal(nx, ny, nz), tex0(u, v) {}
	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex0;
};
struct VertexPosOffsetUV
{
	VertexPosOffsetUV(float x, float y, float z,
		D3DXVECTOR3 offset, float u, float v)
		: pos(x, y, z), offset(offset), tex0(u, v) {}
	D3DXVECTOR3 pos;
	D3DXVECTOR3 offset;
	D3DXVECTOR2 tex0;
};
D3DVERTEXELEMENT9 VertexPosNormalColElements[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_NORMAL, 0 },
	{ 0, 24, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_COLOR, 0 },
	D3DDECL_END()
};
D3DVERTEXELEMENT9 VertexPosNormalUVElements[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_NORMAL, 0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_TEXCOORD, 0 },
	D3DDECL_END()
};
D3DVERTEXELEMENT9 VertexPosOffsetUVElements[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_TEXCOORD, 0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_TEXCOORD, 1 },
	D3DDECL_END()
};
IDirect3DVertexDeclaration9 *VertexPosNormalColDecl = nullptr;
IDirect3DVertexDeclaration9 *VertexPosNormalUVDecl = nullptr;
IDirect3DVertexDeclaration9 *VertexPosOffsetUVDecl = nullptr;

//______________________________________________________________________________
// GLOBALS
// Direct3D
IDirect3D9 *gd3d = nullptr;
IDirect3DDevice9 *gd3dDevice = nullptr;
D3DPRESENT_PARAMETERS gd3dPP = {};

// Input controllers
KeyboardController gKeyboard;
IDirectInput8* gdinput = nullptr;
MouseController* gMouse = nullptr;  // using DirectInput
PadController gPad;  // using xInput

					 // Camera
					 // only used for flycam
float gCameraFlyRadius;
float gCameraFlyAzimuth;
float gCameraFlyPolar;
D3DXVECTOR3 gCameraFlyUp;  // camera Y
D3DXVECTOR3 gCameraFlyFocus;
// only used for 1st person
float gCamera1stPersonAzimuth;
float gCamera1stPersonPolar;
D3DXVECTOR3 gCamera1stPersonPosition;
D3DXVECTOR3 gCamera1stPersonLookDirection;  // camera Z
D3DXVECTOR3 gCamera1stPersonXZLookDirection;  // always along XZ plane
D3DXVECTOR3 gCamera1stPersonUp;  // camera Y

D3DXMATRIX gViewMat;
D3DXMATRIX gProjMat;
D3DXMATRIX gWorldMat;
bool gFirstPerson;

// Application control
HWND ghMainWnd = 0;
int gClientWidth = 1280;
int gClientHeight = 720;
bool gAppCreated;
bool gAppPaused;
bool gFullScreen;
float gTime;
GfxStats *gstats;

// Geometry and material rendering
IDirect3DVertexBuffer9 *gGroundVB;
IDirect3DIndexBuffer9 *gGroundIB;
IDirect3DTexture9 *gGroundTex;
IDirect3DVertexBuffer9 *gTreeVB;
IDirect3DIndexBuffer9 *gTreeIB;
IDirect3DTexture9 *gTreeTex;
IDirect3DVertexBuffer9 *gBrickVB;
IDirect3DIndexBuffer9 *gBrickIB;
IDirect3DVertexBuffer9 *gStudVB;
IDirect3DIndexBuffer9 *gStudIB;

ID3DXEffect *gfx;
D3DXVECTOR3 gLightVecW;
D3DXCOLOR gDiffuseMtrl;
D3DXCOLOR gDiffuseLight;
D3DXCOLOR gAmbientMtrl;
D3DXCOLOR gAmbientLight;
D3DXCOLOR gSpecularMtrl;
D3DXCOLOR gSpecularLight;
float gSpecularPower;
float gBrickSpecularPower;

// LEGO brick properties
const float LEGO_PITCH = 8.0f; // 8mm between studs
const float LEGO_HALF_PITCH = 4.0f; // 8mm between studs
const float LEGO_HEIGHT = 9.6f; // height of a classic brick
const float LEGO_STUD_HEIGHT = 1.8f; // height of a classic brick
const float LEGO_STUD_RADIUS = 2.4f;
const float LEGO_GAP = 0.1f; // clearance gap between bricks

const int NUM_TREES = 4;
int gNumBricks;
const int MAX_BRICKS = 1024;
int gNumStuds;
const int MAX_STUDS = 64000;
const int LEGO_WORLD_HEIGHT = 128;
const int LEGO_WORLD_SIZE = 256;
const float WORLD_SIZE = LEGO_WORLD_SIZE * LEGO_PITCH;
const float WORLD_HALF_SIZE = WORLD_SIZE / 2;
// fast lookup array for one XZ plane of the world to test if space is occupied
bool gHasBrick[LEGO_WORLD_SIZE][LEGO_WORLD_SIZE];  // X,Z

//const int STUD_SUBDIVISIONS_AXIS = 16;
const int STUD_SUBDIVISIONS_AXIS = 16;
const int VERTICES_PER_STUD = STUD_SUBDIVISIONS_AXIS * 3 + 1;
const int TRIANGLES_PER_STUD = STUD_SUBDIVISIONS_AXIS * 3;
const int INDICES_PER_STUD = TRIANGLES_PER_STUD * 3;
D3DXVECTOR3 gLegoStudNormals[STUD_SUBDIVISIONS_AXIS];
D3DXVECTOR3 gLegoStudVertices[STUD_SUBDIVISIONS_AXIS];
bool gLegoStudDataInitialised;

// Structures
struct Brick
{
	int posLegoX;
	int posLegoY;
	int posLegoZ;
	int sizeX;
	int sizeZ;
	D3DCOLOR col;
	void move(int X, int Y, int Z) {
		posLegoX += X;
		posLegoY += Y;
		posLegoZ += Z;
	}
};

struct Stud
{
	int posLegoX;
	int posLegoY;
	int posLegoZ;
	D3DCOLOR col;
};

class StudManager
{
public:
	//StudManager() : prevLayer((std::numeric_limits<int>::max)()) {
	//	ClearEvenOccupiedArray();
	//	ClearOddOccupiedArray();
	//}
	void addStud(Stud s) {
#ifdef STUD_OPTIMISATION
		// Only add stud if space above is unoccupied
		// It is assumed that bricks are added from the top down
		if (s.posLegoY != prevLayer) {
			// Starting a new layer with this stud
			if (s.posLegoY < prevLayer - 1) {
				ClearEvenOccupiedArray();
				ClearOddOccupiedArray();
			}
			else if (prevLayer % 2 == 0) {
				ClearOddOccupiedArray();
			}
			else {
				ClearEvenOccupiedArray();
			}
			prevLayer = s.posLegoY;
		}

		size_t Xindex = s.posLegoX + LEGO_WORLD_SIZE/2 + 1;
		size_t Zindex = s.posLegoZ + LEGO_WORLD_SIZE/2 + 1;
		if (s.posLegoY % 2 == 0) {
			if (!oddLayerOccupied[Xindex][Zindex]) {
				studs.push_back(s);
			}
			evenLayerOccupied[Xindex][Zindex] = true;
		}
		else {
			if (!evenLayerOccupied[Xindex][Zindex]) {
				studs.push_back(s);
			}
			oddLayerOccupied[Xindex][Zindex] = true;
		}
#else
		studs.push_back(s);
#endif
	}

	// Call this before starting a new model to reset layers.
	void newModel() {
		ClearEvenOccupiedArray();
		ClearOddOccupiedArray();
		prevLayer = (std::numeric_limits<int>::max)();
	}

	vector<Stud> studs;
private:
	void ClearEvenOccupiedArray() {
		for (int i = 0; i < LEGO_WORLD_SIZE; ++i) {
			for (int j = 0; j < LEGO_WORLD_SIZE; ++j) {
				evenLayerOccupied[i][j] = false;
			}
		}
	}
	void ClearOddOccupiedArray() {
		for (int i = 0; i < LEGO_WORLD_SIZE; ++i) {
			for (int j = 0; j < LEGO_WORLD_SIZE; ++j) {
				oddLayerOccupied[i][j] = false;
			}
		}
	}
	int prevLayer;
	bool evenLayerOccupied[LEGO_WORLD_SIZE + 2][LEGO_WORLD_SIZE + 2];
	bool oddLayerOccupied[LEGO_WORLD_SIZE + 2][LEGO_WORLD_SIZE + 2];
};
vector<vector<Brick>> gLegoScene;
StudManager *gStuds;

// Handles
D3DXHANDLE ghTime;
D3DXHANDLE ghGroundTech;
D3DXHANDLE ghTreeTech;
D3DXHANDLE ghBrickTech;
D3DXHANDLE ghWVP;
D3DXHANDLE ghViewProj;
D3DXHANDLE ghWorldInverseTranspose;
D3DXHANDLE ghLightVecW;
D3DXHANDLE ghDiffuseMtrl;
D3DXHANDLE ghDiffuseLight;
D3DXHANDLE ghAmbientMtrl;
D3DXHANDLE ghAmbientLight;
D3DXHANDLE ghSpecularMtrl;
D3DXHANDLE ghSpecularLight;
D3DXHANDLE ghSpecularPower;
D3DXHANDLE ghBrickSpecularPower;
D3DXHANDLE ghEyePos;
D3DXHANDLE ghWorld;
D3DXHANDLE ghGroundTex;
D3DXHANDLE ghTreeTex;

//______________________________________________________________________________


//______________________________________________________________________________
// LEGO CREATION
// Write to the vertex and index buffers with the brick geometry starting at the
// buffer indexes given. Indexes are incremented to the end of the data written.
void CreateBrick(int posLegoX, int posLegoY, int posLegoZ, int sizeX, int sizeZ,
	const D3DCOLOR col, VertexPosNormalCol pVB[],
	size_t &VBIndex, DWORD pIB[], size_t &IBIndex) {
	if (gNumBricks < MAX_BRICKS) {
		float leftX = posLegoX * LEGO_PITCH + LEGO_GAP;
		float rightX = (posLegoX + sizeX) * LEGO_PITCH - LEGO_GAP;
		float bottomY = posLegoY * LEGO_HEIGHT;
		float topY = (posLegoY + 1) * LEGO_HEIGHT - 0.05f;  // add a bit of a gap
		float frontZ = posLegoZ * LEGO_PITCH + LEGO_GAP;
		float backZ = (posLegoZ + sizeZ) * LEGO_PITCH - LEGO_GAP;

		D3DXVECTOR3 corner0(leftX, bottomY, frontZ);
		D3DXVECTOR3 corner1(rightX, bottomY, frontZ);
		D3DXVECTOR3 corner2(leftX, bottomY, backZ);
		D3DXVECTOR3 corner3(rightX, bottomY, backZ);
		D3DXVECTOR3 corner4(leftX, topY, frontZ);
		D3DXVECTOR3 corner5(rightX, topY, frontZ);
		D3DXVECTOR3 corner6(leftX, topY, backZ);
		D3DXVECTOR3 corner7(rightX, topY, backZ);

		static const D3DXVECTOR3 normalX(1, 0, 0);
		static const D3DXVECTOR3 normalY(0, 1, 0);
		static const D3DXVECTOR3 normalZ(0, 0, 1);

		int VBIndexBase = VBIndex;
		// front
		pVB[VBIndex++] = VertexPosNormalCol(corner0, -normalZ, col); //0
		pVB[VBIndex++] = VertexPosNormalCol(corner4, -normalZ, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner5, -normalZ, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner1, -normalZ, col);
		// back
		pVB[VBIndex++] = VertexPosNormalCol(corner3, normalZ, col); //4
		pVB[VBIndex++] = VertexPosNormalCol(corner7, normalZ, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner6, normalZ, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner2, normalZ, col);
		// left
		pVB[VBIndex++] = VertexPosNormalCol(corner2, -normalX, col); //8
		pVB[VBIndex++] = VertexPosNormalCol(corner6, -normalX, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner4, -normalX, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner0, -normalX, col);
		// right
		pVB[VBIndex++] = VertexPosNormalCol(corner1, normalX, col); //12
		pVB[VBIndex++] = VertexPosNormalCol(corner5, normalX, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner7, normalX, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner3, normalX, col);
		// top
		pVB[VBIndex++] = VertexPosNormalCol(corner4, normalY, col); //16
		pVB[VBIndex++] = VertexPosNormalCol(corner6, normalY, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner7, normalY, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner5, normalY, col);
		// bottom
		pVB[VBIndex++] = VertexPosNormalCol(corner2, -normalY, col); //20
		pVB[VBIndex++] = VertexPosNormalCol(corner0, -normalY, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner1, -normalY, col);
		pVB[VBIndex++] = VertexPosNormalCol(corner3, -normalY, col);

		// front
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 3;
		VBIndexBase += 4;
		// back
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 3;
		VBIndexBase += 4;
		// left
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 3;
		VBIndexBase += 4;
		// right
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 3;
		VBIndexBase += 4;
		// top
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 3;
		VBIndexBase += 4;
		// bottom
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 2;
		pIB[IBIndex++] = VBIndexBase + 3;

		// Create studs
		for (int x = posLegoX; x < posLegoX + sizeX; ++x) {
			for (int z = posLegoZ; z < posLegoZ + sizeZ; ++z) {
				gStuds->addStud(Stud{ x, posLegoY, z, col });
			}
		}

		++gNumBricks;
		gstats->addVertices(24);
		gstats->addTriangles(36);
	}
}

void CreateBrick(Brick b, VertexPosNormalCol pVB[],
	size_t &VBIndex, DWORD pIB[], size_t &IBIndex) {
	CreateBrick(b.posLegoX, b.posLegoY, b.posLegoZ, b.sizeX, b.sizeZ,
		b.col, pVB, VBIndex, pIB, IBIndex);
}

// Make sure to call this (at least) once before CreateStud().
// Calculate normals in the XZ plane once, then re-use for all studs.
void InitialiseStudStaticData() {
	float angle;
	float cosTheta;
	float sinTheta;
	for (int i = 0; i < STUD_SUBDIVISIONS_AXIS; ++i) {
		angle = i * 2 * D3DX_PI / STUD_SUBDIVISIONS_AXIS;
		cosTheta = cosf(angle);
		sinTheta = sinf(angle);
		// Stud normals
		gLegoStudNormals[i] = D3DXVECTOR3(cosTheta, 0, sinTheta);
		// Stud perimeter vertex offsets from center
		gLegoStudVertices[i] = D3DXVECTOR3(cosTheta * LEGO_STUD_RADIUS,
			0, sinTheta * LEGO_STUD_RADIUS);
	}
	gLegoStudDataInitialised = true;  // Not used yet, but maybe useful...
}

// Write to the vertex and index buffers with the stud geometry starting at the
// buffer indexes given. Indexes are incremented to the end of the data written.
void CreateStud(int posLegoX, int posLegoY, int posLegoZ, const D3DCOLOR col,
	VertexPosNormalCol pVB[], size_t &VBIndex, DWORD pIB[],
	size_t &IBIndex) {
	if (gNumStuds <= MAX_STUDS) {
		float centerX = posLegoX * LEGO_PITCH + LEGO_HALF_PITCH;
		float centerZ = posLegoZ * LEGO_PITCH + LEGO_HALF_PITCH;
		float bottomY = (posLegoY + 1) * LEGO_HEIGHT - 1.0f;  // add a bit of overlap
		float topY = (posLegoY + 1) * LEGO_HEIGHT + LEGO_STUD_HEIGHT;

		D3DXVECTOR3 topCenter(centerX, topY, centerZ);
		D3DXVECTOR3 bottomCenter(centerX, bottomY, centerZ);

		// top cap
		int VBIndexBase = VBIndex;
		pVB[VBIndex++] = VertexPosNormalCol(topCenter, D3DXVECTOR3(0, 1, 0), col);
		for (int i = 0; i < STUD_SUBDIVISIONS_AXIS - 1; ++i) {
			pVB[VBIndex++] = VertexPosNormalCol(
				topCenter + gLegoStudVertices[i], D3DXVECTOR3(0, 1, 0), col);
			pIB[IBIndex++] = VBIndexBase + 0;
			pIB[IBIndex++] = VBIndexBase + i + 2;
			pIB[IBIndex++] = VBIndexBase + i + 1;
		}
		pVB[VBIndex++] = VertexPosNormalCol(
			topCenter + gLegoStudVertices[STUD_SUBDIVISIONS_AXIS - 1],
			D3DXVECTOR3(0, 1, 0), col);
		pIB[IBIndex++] = VBIndexBase + 0;
		pIB[IBIndex++] = VBIndexBase + 1;
		pIB[IBIndex++] = VBIndexBase + STUD_SUBDIVISIONS_AXIS;


		// side wall
		VBIndexBase = VBIndex;
		for (int i = 0; i < STUD_SUBDIVISIONS_AXIS - 1; ++i) {
			pVB[VBIndex++] = VertexPosNormalCol(
				topCenter + gLegoStudVertices[i], gLegoStudNormals[i], col);
			pVB[VBIndex++] = VertexPosNormalCol(
				bottomCenter + gLegoStudVertices[i], gLegoStudNormals[i], col);
			pIB[IBIndex++] = VBIndexBase + i * 2 + 0;
			pIB[IBIndex++] = VBIndexBase + i * 2 + 2;
			pIB[IBIndex++] = VBIndexBase + i * 2 + 1;
			pIB[IBIndex++] = VBIndexBase + i * 2 + 1;
			pIB[IBIndex++] = VBIndexBase + i * 2 + 2;
			pIB[IBIndex++] = VBIndexBase + i * 2 + 3;
		}
		pVB[VBIndex++] = VertexPosNormalCol(
			topCenter + gLegoStudVertices[STUD_SUBDIVISIONS_AXIS - 1],
			gLegoStudNormals[STUD_SUBDIVISIONS_AXIS - 1], col);
		pVB[VBIndex++] = VertexPosNormalCol(
			bottomCenter + gLegoStudVertices[STUD_SUBDIVISIONS_AXIS - 1],
			gLegoStudNormals[STUD_SUBDIVISIONS_AXIS - 1], col);
		pIB[IBIndex++] = VBIndexBase + (STUD_SUBDIVISIONS_AXIS - 1) * 2 + 0;
		pIB[IBIndex++] = VBIndexBase;
		pIB[IBIndex++] = VBIndexBase + (STUD_SUBDIVISIONS_AXIS - 1) * 2 + 1;
		pIB[IBIndex++] = VBIndexBase + (STUD_SUBDIVISIONS_AXIS - 1) * 2 + 1;
		pIB[IBIndex++] = VBIndexBase;
		pIB[IBIndex++] = VBIndexBase + 1;

		++gNumStuds;
		gstats->addVertices(VERTICES_PER_STUD);
		gstats->addTriangles(TRIANGLES_PER_STUD);
	}
}

void CreateStud(Stud s, VertexPosNormalCol pVB[], size_t &VBIndex, DWORD pIB[],
				size_t &IBIndex) {
	CreateStud(s.posLegoX, s.posLegoY, s.posLegoZ, s.col, pVB, VBIndex, pIB,
			   IBIndex);
}

// Fill gBricks with a 2D array of bricks
void PopulateBrickSheet(int posLegoX, int posLegoY, int posLegoZ, int numX, int numZ,
	int sizeX, int sizeZ, D3DCOLOR col, vector<Brick> &bricks) {
	for (int x = 0; x < numX; ++x) {
		for (int z = 0; z < numZ; ++z) {
			bricks.push_back(Brick{ posLegoX + sizeX*x, posLegoY,
				posLegoZ + sizeZ*z, sizeX, sizeZ, col });
		}
	}
}

// Add a row of bricks in the X direction
void PopulateBrickRowX(int posLegoX, int posLegoY, int posLegoZ, int numX,
	int sizeX, int sizeZ, D3DCOLOR col, vector<Brick> &bricks) {
	for (int x = 0; x < numX; ++x) {
		bricks.push_back(Brick{ posLegoX + sizeX*x, posLegoY,
			posLegoZ, sizeX, sizeZ, col });
	}
}

// Add a row (well, column) of bricks in the Y direction
void PopulateBrickRowY(int posLegoX, int posLegoY, int posLegoZ, int numY,
					   int sizeX, int sizeZ, D3DCOLOR col, vector<Brick> &bricks) {
	for (int y = 0; y < numY; ++y) {
		bricks.push_back(Brick{ posLegoX, posLegoY + y,
						 posLegoZ, sizeX, sizeZ, col });
	}
}

// Add a row of bricks in the Z direction
void PopulateBrickRowZ(int posLegoX, int posLegoY, int posLegoZ, int numZ,
	int sizeX, int sizeZ, D3DCOLOR col, vector<Brick> &bricks) {
	for (int z = 0; z < numZ; ++z) {
		bricks.push_back(Brick{ posLegoX, posLegoY,
			posLegoZ + sizeZ*z, sizeX, sizeZ, col });
	}
}

// Read in brick data from my custom Lego Scene Description (LSD) file and 
// WARNING: NO ERROR CHECKING on data lines!
void ReadLSDFile(const string &filename, vector<Brick> &model,
				 const int posOffsetX, const int posOffsetY, 
				 const int posOffsetZ) {
	ifstream lsdIn(filename);
	string prefix;
	int posX, posY, posZ, number, sizeX, sizeZ;
	string colour;
	D3DCOLOR col;
	lsdIn >> prefix;
	while (lsdIn) {
		if (prefix[0] == 'b') {
			// brick
			lsdIn >> posX >> posY >> posZ >> sizeX >> sizeZ >> colour;
			posX += posOffsetX;
			posY += posOffsetY;
			posZ += posOffsetZ;
			col = InterpretColor(colour);
			model.push_back(Brick{ posX, posY, posZ, sizeX, sizeZ, col });
		}
		else if (prefix[0] == 'r') {
			// brick row
			lsdIn >> posX >> posY >> posZ >> number
				>> sizeX >> sizeZ >> colour;
			posX += posOffsetX;
			posY += posOffsetY;
			posZ += posOffsetZ;
			col = InterpretColor(colour);
			if (prefix[1] == 'x') {
				PopulateBrickRowX(posX, posY, posZ, number,
								  sizeX, sizeZ, col, model);
			}
			else if (prefix[1] == 'y') {
				PopulateBrickRowY(posX, posY, posZ, number,
								  sizeX, sizeZ, col, model);
			}
			else if (prefix[1] == 'z') {
				PopulateBrickRowZ(posX, posY, posZ, number,
								  sizeX, sizeZ, col, model);
			}
			else {
				lsdIn.ignore((std::numeric_limits<int>::max)(),
								'\n'); // skip to next line
			}
		}
		else {
			lsdIn.ignore((std::numeric_limits<int>::max)(),
							'\n'); // skip to next line
		}
		lsdIn >> prefix;
	}
	lsdIn.close();
}

void ReadLSDFile(const string &filename, vector<Brick> &model) {
	ReadLSDFile(filename, model, 0, 0, 0);
}

//______________________________________________________________________________
// APPLICATION SPECIFIC FUNCTIONS
void initializeApp() {
	{
		gstats = new GfxStats();
		gStuds = new StudManager();

		// Initial camera positions
		// Flycam
		gCameraFlyRadius = 420.0f;
		gCameraFlyAzimuth = 5.3f;
		gCameraFlyPolar = 0.63f;
		gCameraFlyFocus = { 228, 50, 30 };
		gCameraFlyUp = { 0, 1, 0 };

		// 1st person cam
		gFirstPerson = false;
		gCamera1stPersonAzimuth = 0.0f;
		gCamera1stPersonPolar = 0.0f;
		gCamera1stPersonPosition = { 0, 40, 0 };
		gCamera1stPersonUp = { 0, 1, 0 };

		gLightVecW = D3DXVECTOR3(0.4f, 0.25f, 1);
		//gLightVecW = D3DXVECTOR3(-0.4f, 0.25f, -1);

		gDiffuseMtrl = D3DXCOLOR(1, 1, 1, 1);
		gDiffuseLight = D3DXCOLOR(0.9f, 0.92f, 0.94f, 1);
		gAmbientMtrl = D3DXCOLOR(1, 1, 1, 1);
		gAmbientLight = D3DXCOLOR(0.3f, 0.35f, 0.4f, 1.0f);
		gSpecularMtrl = D3DXCOLOR(0.3f, 0.3f, 0.3f, 0.f);
		gSpecularLight = D3DXCOLOR(1, 1, 1, 1);
		gSpecularPower = 8;
		gBrickSpecularPower = 16;

		D3DXMatrixIdentity(&gWorldMat);
	}

	//_____________________________________________________________
	// Build ground
	{
		WORD *pI16 = nullptr;
#ifdef TEXTURED_GROUND
		HR(gd3dDevice->CreateVertexBuffer(4 * sizeof(VertexPosNormalUV),
			D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED,
			&gGroundVB, 0));
		VertexPosNormalUV *pVB1 = nullptr;
		HR(gGroundVB->Lock(0, 0, (void **)&pVB1, 0));
		pVB1[0] = VertexPosNormalUV(-WORLD_HALF_SIZE, 0.0f, -WORLD_HALF_SIZE,
			0.0f, 1.0f, 0.0f, 5.0f, 5.0f);
		pVB1[1] = VertexPosNormalUV(WORLD_HALF_SIZE, 0.0f, WORLD_HALF_SIZE,
			0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
		pVB1[2] = VertexPosNormalUV(WORLD_HALF_SIZE, 0.0f, -WORLD_HALF_SIZE,
			0.0f, 1.0f, 0.0f, 0.0f, 5.0f);
		pVB1[3] = VertexPosNormalUV(-WORLD_HALF_SIZE, 0.0f, WORLD_HALF_SIZE,
			0.0f, 1.0f, 0.0f, 5.0f, 0.0f);
		HR(gGroundVB->Unlock());

		// Create the index buffer.
		HR(gd3dDevice->CreateIndexBuffer(6 * sizeof(WORD), D3DUSAGE_WRITEONLY,
			D3DFMT_INDEX16, D3DPOOL_MANAGED,
			&gGroundIB, 0));
		HR(gGroundIB->Lock(0, 0, (void **)&pI16, 0));
		pI16[0] = 0;    pI16[1] = 1;    pI16[2] = 2;
		pI16[3] = 0;    pI16[4] = 3;    pI16[5] = 1;
		HR(gGroundIB->Unlock());

		HR(D3DXCreateTextureFromFile(gd3dDevice, _T("ground0.dds"), &gGroundTex));
#endif
	}

	//_____________________________________________________________
	// Build trees
	{
#ifdef TEXTURED_TREES
		HR(gd3dDevice->CreateVertexBuffer(4 * NUM_TREES * sizeof(VertexPosOffsetUV),
			D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED,
			&gTreeVB, 0));
		VertexPosOffsetUV *pVB2 = nullptr;
		HR(gTreeVB->Lock(0, 0, (void **)&pVB2, 0));
		const float TREE_SIZE = 100.0f;
		const float Y_OFFSET = -5.0f;

		D3DXVECTOR3 offset(-140, Y_OFFSET, -100);
		pVB2[0] = VertexPosOffsetUV(-TREE_SIZE / 2, 0.0f, 0.0f, offset, 0.0f, 1.0f);
		pVB2[1] =
			VertexPosOffsetUV(-TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 0.0f, 0.0f);
		pVB2[2] =
			VertexPosOffsetUV(TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 1.0f, 0.0f);
		pVB2[3] = VertexPosOffsetUV(TREE_SIZE / 2, 0.0f, 0.0f, offset, 1.0f, 1.0f);

		offset = { -50, Y_OFFSET, 150 };
		pVB2[4] = VertexPosOffsetUV(-TREE_SIZE / 2, 0.0f, 0.0f, offset, 0.0f, 1.0f);
		pVB2[5] =
			VertexPosOffsetUV(-TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 0.0f, 0.0f);
		pVB2[6] =
			VertexPosOffsetUV(TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 1.0f, 0.0f);
		pVB2[7] = VertexPosOffsetUV(TREE_SIZE / 2, 0.0f, 0.0f, offset, 1.0f, 1.0f);

		offset = { 60, Y_OFFSET, 140 };
		pVB2[8] = VertexPosOffsetUV(-TREE_SIZE / 2, 0.0f, 0.0f, offset, 0.0f, 1.0f);
		pVB2[9] =
			VertexPosOffsetUV(-TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 0.0f, 0.0f);
		pVB2[10] =
			VertexPosOffsetUV(TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 1.0f, 0.0f);
		pVB2[11] = VertexPosOffsetUV(TREE_SIZE / 2, 0.0f, 0.0f, offset, 1.0f, 1.0f);

		offset = { 140, Y_OFFSET, 80 };
		pVB2[12] =
			VertexPosOffsetUV(-TREE_SIZE / 2, 0.0f, 0.0f, offset, 0.0f, 1.0f);
		pVB2[13] =
			VertexPosOffsetUV(-TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 0.0f, 0.0f);
		pVB2[14] =
			VertexPosOffsetUV(TREE_SIZE / 2, TREE_SIZE, 0.0f, offset, 1.0f, 0.0f);
		pVB2[15] = VertexPosOffsetUV(TREE_SIZE / 2, 0.0f, 0.0f, offset, 1.0f, 1.0f);
		HR(gTreeVB->Unlock());

		// Create the index buffer.
		HR(gd3dDevice->CreateIndexBuffer(6 * NUM_TREES * sizeof(WORD),
			D3DUSAGE_WRITEONLY, D3DFMT_INDEX16,
			D3DPOOL_MANAGED, &gTreeIB, 0));
		pI16 = nullptr;
		HR(gTreeIB->Lock(0, 0, (void **)&pI16, 0));
		pI16[0] = 0;    pI16[1] = 1;    pI16[2] = 2;
		pI16[3] = 0;    pI16[4] = 2;    pI16[5] = 3;
		pI16[6] = 4;    pI16[7] = 5;    pI16[8] = 6;
		pI16[9] = 4;    pI16[10] = 6;   pI16[11] = 7;
		pI16[12] = 8;   pI16[13] = 9;   pI16[14] = 10;
		pI16[15] = 8;   pI16[16] = 10;  pI16[17] = 11;
		pI16[18] = 12;  pI16[19] = 13;  pI16[20] = 14;
		pI16[21] = 12;  pI16[22] = 14;  pI16[23] = 15;
		HR(gTreeIB->Unlock());

		HR(D3DXCreateTextureFromFile(gd3dDevice, _T("tree.dds"), &gTreeTex));
#endif
	}

	//_____________________________________________________________
	// Build bricks
	{
		HR(gd3dDevice->CreateVertexBuffer(24 * MAX_BRICKS * sizeof(VertexPosNormalCol),
			D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED,
			&gBrickVB, 0));
		HR(gd3dDevice->CreateIndexBuffer(36 * MAX_BRICKS * sizeof(DWORD),
			D3DUSAGE_WRITEONLY, D3DFMT_INDEX32,
			D3DPOOL_MANAGED, &gBrickIB, 0));

		VertexPosNormalCol *pVB3 = nullptr;
		HR(gBrickVB->Lock(0, 0, (void **)&pVB3, 0));
		size_t VBIndex = 0;

		DWORD *pI32 = nullptr;
		HR(gBrickIB->Lock(0, 0, (void **)&pI32, 0));
		size_t IBIndex = 0;

		ifstream modelList("modelList.txt");
		string filename;
		getline(modelList, filename);
		while (modelList) {
			vector<Brick> model;
			ReadLSDFile(filename, model);
			gLegoScene.push_back(model);
			getline(modelList, filename);
		}

#ifdef STUD_OPTIMISATION
		// Sort bricks in each model in gLegoScene by y coordinate descending
		for (auto &model : gLegoScene) {
			sort(model.begin(), model.end(),
				 [](const Brick &a, const Brick &b) {
				return a.posLegoY > b.posLegoY; });
		}
#endif

		// Add each brick queued up in gLegoScene to vertex and index buffers
		for (auto const &model : gLegoScene) {
			gStuds->newModel();
			for (auto const &b : model) {
				CreateBrick(b, pVB3, VBIndex, pI32, IBIndex);
			}
		}

		HR(gBrickVB->Unlock());
		HR(gBrickIB->Unlock());
	}

	//_____________________________________________________________
	// Build studs
	{
		InitialiseStudStaticData();
		HR(gd3dDevice->CreateVertexBuffer(VERTICES_PER_STUD * MAX_STUDS *
			sizeof(VertexPosNormalCol),
			D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED,
			&gStudVB, 0));
		HR(gd3dDevice->CreateIndexBuffer(INDICES_PER_STUD * MAX_STUDS *
			sizeof(DWORD),
			D3DUSAGE_WRITEONLY, D3DFMT_INDEX32,
			D3DPOOL_MANAGED, &gStudIB, 0));

		VertexPosNormalCol *pVB4 = nullptr;
		HR(gStudVB->Lock(0, 0, (void **)&pVB4, 0));
		size_t VBIndex = 0;

		DWORD *pI32 = nullptr;
		HR(gStudIB->Lock(0, 0, (void **)&pI32, 0));
		size_t IBIndex = 0;

		// Populate buffers
		for (Stud s : gStuds->studs) {
			CreateStud(s, pVB4, VBIndex, pI32, IBIndex);
		}

		HR(gStudVB->Unlock());
		HR(gStudIB->Unlock());
	}

	//_____________________________________________________________
	// Build fx
	{
		ID3DXBuffer* errors = nullptr;

		HR(D3DXCreateEffectFromFile(gd3dDevice, _T("dirLightTex.fx"), 0, 0,
			D3DXSHADER_DEBUG, 0, &gfx, &errors));
		if (errors) {
			MessageBox(0, (wchar_t*)errors->GetBufferPointer(), 0, 0);
		}

		// Obtain handles
		ghTime = gfx->GetParameterByName(0, "gTime");
		ghWVP = gfx->GetParameterByName(0, "gWVP");
		ghViewProj = gfx->GetParameterByName(0, "gViewProj");
		ghWorldInverseTranspose =
			gfx->GetParameterByName(0, "gWorldInverseTranspose");
		ghLightVecW = gfx->GetParameterByName(0, "gLightVecW");
		ghDiffuseMtrl = gfx->GetParameterByName(0, "gDiffuseMtrl");
		ghDiffuseLight = gfx->GetParameterByName(0, "gDiffuseLight");
		ghAmbientMtrl = gfx->GetParameterByName(0, "gAmbientMtrl");
		ghAmbientLight = gfx->GetParameterByName(0, "gAmbientLight");
		ghSpecularMtrl = gfx->GetParameterByName(0, "gSpecularMtrl");
		ghSpecularLight = gfx->GetParameterByName(0, "gSpecularLight");
		ghSpecularPower = gfx->GetParameterByName(0, "gSpecularPower");
		ghBrickSpecularPower = gfx->GetParameterByName(0, "gBrickSpecularPower");
		ghEyePos = gfx->GetParameterByName(0, "gEyePosW");
		ghWorld = gfx->GetParameterByName(0, "gWorld");
		ghGroundTech = gfx->GetTechniqueByName("DirLightTexTech");
		ghTreeTech = gfx->GetTechniqueByName("TreeTech");
		ghBrickTech = gfx->GetTechniqueByName("BrickTech");
		ghGroundTex = gfx->GetParameterByName(0, "gGroundTex");
		ghTreeTex = gfx->GetParameterByName(0, "gTreeTex");
	}

	//_____________________________________________________________
	// Set the device state to initial conditions
	onResetDevice();

	// Initialise vertex declarations
	HR(gd3dDevice->CreateVertexDeclaration(VertexPosNormalColElements, &VertexPosNormalColDecl));
	HR(gd3dDevice->CreateVertexDeclaration(VertexPosNormalUVElements, &VertexPosNormalUVDecl));
	HR(gd3dDevice->CreateVertexDeclaration(VertexPosOffsetUVElements, &VertexPosOffsetUVDecl));
}

void shutdownApp() {
	delete gstats;
	delete gStuds;
	ReleaseCOM(gfx);
#ifdef TEXTURED_GROUND
	ReleaseCOM(gGroundVB);
	ReleaseCOM(gGroundIB);
	ReleaseCOM(gGroundTex);
#endif
#ifdef TEXTURED_TREES
	ReleaseCOM(gTreeVB);
	ReleaseCOM(gTreeIB);
	ReleaseCOM(gTreeTex);
#endif
	ReleaseCOM(gBrickVB);
	ReleaseCOM(gBrickIB);
	ReleaseCOM(gStudVB);
	ReleaseCOM(gStudIB);

	ReleaseCOM(VertexPosNormalColDecl);
	ReleaseCOM(VertexPosNormalUVDecl);
	ReleaseCOM(VertexPosOffsetUVDecl);

	ReleaseCOM(gdinput);
}

void updateScene(float dt) {
	gMouse->poll();
	gstats->update(dt);
	gTime += dt;

	// Fullscreen toggle
	if (gKeyboard.esc)
		enableFullScreenMode(false);
	else if (gKeyboard.f)
		if (!gFullScreen) {
			enableFullScreenMode(true);
			gFullScreen = true;
		}
		else {
			enableFullScreenMode(false);
			gFullScreen = false;
		}

	// Camera toggle
	static bool cAlreadyDown = false;
	if (gKeyboard.c) {
		if (!cAlreadyDown) {
			gFirstPerson = !gFirstPerson;
			cAlreadyDown = true;
		}
	}
	else {
		cAlreadyDown = false;
	}

	// Move camera
	if (!gFirstPerson) {
		// Flycam
		static const float CAMERA_FLY_SPEED = 200.0f;
		static const float MOUSE_MOVE_X_SENSITIVITY = 0.004f;
		static const float MOUSE_MOVE_Y_SENSITIVITY = 0.004f;
		static const float MOUSE_ROTATE_X_SENSITIVITY = 0.005f;
		static const float MOUSE_ROTATE_Y_SENSITIVITY = 0.005f;
		static const float MOUSE_ZOOM_SENSITIVITY = 0.008f;

		// Calculate vectors lookXZ and cameraRight used for camera movement
		float x = cosf(gCameraFlyPolar) * cosf(gCameraFlyAzimuth);
		float y = sinf(gCameraFlyPolar);
		float z = cosf(gCameraFlyPolar) * sinf(gCameraFlyAzimuth);
		D3DXVECTOR3 lookXZ(-x, 0, -z);
		D3DXVec3Normalize(&lookXZ, &lookXZ);
		D3DXVECTOR3 cameraRight;
		D3DXVec3Cross(&cameraRight, &gCameraFlyUp, &lookXZ);

		static bool wut = true;;
		if (gMouse->leftdown()) {
			D3DXVECTOR3 moveDelta = { 0, 0, 0 };
			moveDelta -= cameraRight * gMouse->dx() * gCameraFlyRadius * 
				MOUSE_MOVE_X_SENSITIVITY;
			moveDelta += lookXZ * gMouse->dy() * gCameraFlyRadius * 
				MOUSE_MOVE_Y_SENSITIVITY;
			
			gCameraFlyFocus += moveDelta;
		}
		else if (gMouse->middledown()) {
			gCameraFlyRadius *= 1 - gMouse->dx() * MOUSE_ZOOM_SENSITIVITY;
			gCameraFlyRadius *= 1 + gMouse->dy() * MOUSE_ZOOM_SENSITIVITY;
			// Don't let radius get too small.
			if (gCameraFlyRadius < 25.0f)
				gCameraFlyRadius = 25.0f;
		}
		else if (gMouse->rightdown() || wut) {
			wut = false;
			gCameraFlyAzimuth -= gMouse->dx() * MOUSE_ROTATE_X_SENSITIVITY;
			gCameraFlyPolar += gMouse->dy() * MOUSE_ROTATE_Y_SENSITIVITY;
			if (gCameraFlyPolar > D3DX_PI / 2 - 0.01f) {
				gCameraFlyPolar = D3DX_PI / 2 - 0.01f;
			}
			if (gCameraFlyPolar < -(D3DX_PI / 2 - 0.01f)) {
				gCameraFlyPolar = -(D3DX_PI / 2 - 0.01f);
			}
			if (gCameraFlyAzimuth > 2 * D3DX_PI) {
				gCameraFlyAzimuth -= 2 * D3DX_PI;
			}
			if (gCameraFlyAzimuth < 0) {
				gCameraFlyAzimuth += 2 * D3DX_PI;
			}
		}

		// Update the camera position and focus point
		D3DXVECTOR3 cameraVelocity = {0, 0, 0};
		if (gKeyboard.q)
			cameraVelocity.y += 1;
		if (gKeyboard.e)
			cameraVelocity.y -= 1;
		if (gKeyboard.w)
			cameraVelocity += lookXZ;
		if (gKeyboard.s)
			cameraVelocity -= lookXZ;
		if (gKeyboard.a)
			cameraVelocity -= cameraRight;
		if (gKeyboard.d)
			cameraVelocity += cameraRight;
		gCameraFlyFocus += cameraVelocity * CAMERA_FLY_SPEED * dt;
		D3DXVECTOR3 pos(x, y, z);
		pos = pos * gCameraFlyRadius + gCameraFlyFocus;

		// Build global view matrix
		D3DXMatrixLookAtLH(&gViewMat, &pos, &gCameraFlyFocus, &gCameraFlyUp);
		HR(gfx->SetValue(ghEyePos, &pos, sizeof(D3DXVECTOR3)));
	}
	else {
		// 1st person cam
		// mouse look
		static const float MOUSE_1STPERSON_X_SENSITIVITY = 0.003f;
		static const float MOUSE_1STPERSON_Y_SENSITIVITY = 0.003f;

		gCamera1stPersonAzimuth -= gMouse->dx() * MOUSE_1STPERSON_X_SENSITIVITY;
		gCamera1stPersonPolar -= gMouse->dy() * MOUSE_1STPERSON_Y_SENSITIVITY;
		if (gCamera1stPersonPolar > D3DX_PI / 2 - 0.01f) {
			gCamera1stPersonPolar = D3DX_PI / 2 - 0.01f;
		}
		if (gCamera1stPersonPolar < -(D3DX_PI / 2 - 0.01f)) {
			gCamera1stPersonPolar = -(D3DX_PI / 2 - 0.01f);
		}
		if (gCamera1stPersonAzimuth > 2 * D3DX_PI) {
			gCamera1stPersonAzimuth -= 2 * D3DX_PI;
		}
		if (gCamera1stPersonAzimuth < 0) {
			gCamera1stPersonAzimuth += 2 * D3DX_PI;
		}
		D3DXVECTOR3 gCamera1stPersonLookDirection;
		gCamera1stPersonLookDirection.x = 
			cosf(gCamera1stPersonPolar) * cosf(gCamera1stPersonAzimuth);
		gCamera1stPersonLookDirection.y = sinf(gCamera1stPersonPolar);
		gCamera1stPersonLookDirection.z = 
			cosf(gCamera1stPersonPolar) * sinf(gCamera1stPersonAzimuth);
		gCamera1stPersonXZLookDirection.x = cosf(gCamera1stPersonAzimuth);
		gCamera1stPersonXZLookDirection.y = 0;
		gCamera1stPersonXZLookDirection.z = sinf(gCamera1stPersonAzimuth);
		D3DXVECTOR3 cameraRight;
		D3DXVec3Cross(&cameraRight, &gCamera1stPersonUp,
					  &gCamera1stPersonLookDirection);

		// keyboard walk
		static const float CAMERA_WALK_SPEED = 200.0f;
		D3DXVECTOR3 cameraVelocity = {0, 0, 0};
		if (gKeyboard.q)
			cameraVelocity.y += 1;
		if (gKeyboard.e)
			cameraVelocity.y -= 1;
		if (gKeyboard.w)
			cameraVelocity += gCamera1stPersonXZLookDirection; // unit vector
		if (gKeyboard.s)
			cameraVelocity -= gCamera1stPersonXZLookDirection;
		if (gKeyboard.a)
			cameraVelocity -= cameraRight; // unit vector
		if (gKeyboard.d)
			cameraVelocity += cameraRight;
		D3DXVec3Normalize(&cameraVelocity, &cameraVelocity);
		gCamera1stPersonPosition += cameraVelocity * CAMERA_WALK_SPEED * dt;

		D3DXVECTOR3 target = 
			gCamera1stPersonPosition + gCamera1stPersonLookDirection;
		D3DXMatrixLookAtLH(&gViewMat, &gCamera1stPersonPosition, &target, 
						   &gCamera1stPersonUp);
		HR(gfx->SetValue(ghEyePos, &gCamera1stPersonPosition, 
						 sizeof(D3DXVECTOR3)));
	}
}

void drawScene() {
	HR(gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_XRGB(180, 222, 250), 1.0f, 0));

	//_____________________________________________________________
	// BEGIN SCENE
	HR(gd3dDevice->BeginScene());

	// Send global values to FX
	D3DXMATRIX viewProj = gViewMat * gProjMat;
	D3DXMATRIX wvp = gWorldMat * viewProj;
	HR(gfx->SetMatrix(ghWVP, &wvp));

	D3DXMATRIX worldInverseTranspose;
	D3DXMatrixInverse(&worldInverseTranspose, nullptr, &gWorldMat);
	D3DXMatrixTranspose(&worldInverseTranspose, &worldInverseTranspose);

	HR(gfx->SetMatrix(ghWVP, &wvp));
	HR(gfx->SetMatrix(ghViewProj, &viewProj));
	HR(gfx->SetMatrix(ghWorldInverseTranspose, &worldInverseTranspose));
	HR(gfx->SetValue(ghLightVecW, &gLightVecW, sizeof(D3DXVECTOR3)));
	HR(gfx->SetValue(ghDiffuseMtrl, &gDiffuseMtrl, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghDiffuseLight, &gDiffuseLight, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghAmbientMtrl, &gAmbientMtrl, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghAmbientLight, &gAmbientLight, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghSpecularMtrl, &gSpecularMtrl, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghSpecularLight, &gSpecularLight, sizeof(D3DXCOLOR)));
	HR(gfx->SetFloat(ghSpecularPower, gSpecularPower));
	HR(gfx->SetFloat(ghBrickSpecularPower, gBrickSpecularPower));
	HR(gfx->SetMatrix(ghWorld, &gWorldMat));
	HR(gfx->SetFloat(ghTime, gTime));
	UINT numPasses;

	// GROUND
#ifdef TEXTURED_GROUND
	HR(gd3dDevice->SetVertexDeclaration(VertexPosNormalUVDecl));
	HR(gd3dDevice->SetStreamSource(0, gGroundVB, 0, sizeof(VertexPosNormalUV)));
	HR(gd3dDevice->SetIndices(gGroundIB));
	HR(gfx->SetTechnique(ghGroundTech));
	HR(gfx->SetTexture(ghGroundTex, gGroundTex));
	// Begin ground passes
	HR(gfx->Begin(&numPasses, 0));
	for (UINT i = 0; i < numPasses; ++i) {
		HR(gfx->BeginPass(i));
		HR(gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2));
		HR(gfx->EndPass());
	}
	HR(gfx->End());
#endif

	// TREES
#ifdef TEXTURED_TREES
	HR(gd3dDevice->SetVertexDeclaration(VertexPosOffsetUVDecl));
	HR(gd3dDevice->SetStreamSource(0, gTreeVB, 0, sizeof(VertexPosOffsetUV)));
	HR(gd3dDevice->SetIndices(gTreeIB));
	HR(gfx->SetTechnique(ghTreeTech));
	HR(gfx->SetTexture(ghTreeTex, gTreeTex));
	// Begin tree passes
	HR(gfx->Begin(&numPasses, 0));
	for (UINT i = 0; i < numPasses; ++i) {
		HR(gfx->BeginPass(i));
		HR(gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0,
			4 * NUM_TREES, 0, 2 * NUM_TREES));
		HR(gfx->EndPass());
	}
	HR(gfx->End());
#endif

	// BRICKS
	HR(gd3dDevice->SetVertexDeclaration(VertexPosNormalColDecl));
	HR(gd3dDevice->SetStreamSource(0, gBrickVB, 0, sizeof(VertexPosNormalCol)));
	HR(gd3dDevice->SetIndices(gBrickIB));
	HR(gfx->SetTechnique(ghBrickTech));
	// Begin brick passes
	HR(gfx->Begin(&numPasses, 0));
	for (UINT i = 0; i < numPasses; ++i) {
		HR(gfx->BeginPass(i));
		HR(gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0,
			24 * gNumBricks, 0, 12 * gNumBricks));
		HR(gfx->EndPass());
	}
	HR(gfx->End());

	// STUDS
	HR(gd3dDevice->SetVertexDeclaration(VertexPosNormalColDecl));
	HR(gd3dDevice->SetStreamSource(0, gStudVB, 0, sizeof(VertexPosNormalCol)));
	HR(gd3dDevice->SetIndices(gStudIB));
	HR(gfx->SetTechnique(ghBrickTech));
	// Begin stud passes
	HR(gfx->Begin(&numPasses, 0));
	for (UINT i = 0; i < numPasses; ++i) {
		HR(gfx->BeginPass(i));
		HR(gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0,
			VERTICES_PER_STUD * gNumStuds, 0,
			TRIANGLES_PER_STUD * gNumStuds));
		HR(gfx->EndPass());
	}
	HR(gfx->End());

	//_____________________________________________________________
	// END SCENE
	HR(gd3dDevice->EndScene());

	gstats->display();
	HR(gd3dDevice->Present(0, 0, 0, 0));
}

void onLostDevice() {
	HR(gfx->OnLostDevice());
	gstats->onLostDevice();
}

void onResetDevice() {
	HR(gfx->OnResetDevice());
	gstats->onResetDevice();

	// Build global projection matrix
	float w = (float)gd3dPP.BackBufferWidth;
	float h = (float)gd3dPP.BackBufferHeight;
	D3DXMatrixPerspectiveFovLH(&gProjMat, D3DX_PI * 0.25f, w / h, 
							   0.1f, WORLD_SIZE * 5);

	//gd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
}

//_________________________________________________________________
// GENERAL FUNCTIONS
void enableFullScreenMode(bool enable) {
	// Switch to fullscreen mode.
	if (enable) {
		// Are we already in fullscreen mode?
		if (!gd3dPP.Windowed)
			return;

		int width = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		gd3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
		gd3dPP.BackBufferWidth = width;
		gd3dPP.BackBufferHeight = height;
		gd3dPP.Windowed = false;

		// Change the window style to a more fullscreen friendly style.
		SetWindowLongPtr(ghMainWnd, GWL_STYLE, WS_POPUP);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(ghMainWnd, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);
	}
	// Switch to windowed mode.
	else {
		// Are we already in windowed mode?
		if (gd3dPP.Windowed) {
			return;
		}

		RECT R = { 0, 0, gClientWidth, gClientHeight };
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
		gd3dPP.BackBufferFormat = D3DFMT_UNKNOWN;
		gd3dPP.BackBufferWidth = gClientWidth;
		gd3dPP.BackBufferHeight = gClientHeight;
		gd3dPP.Windowed = true;

		// Change the window style to a more windowed friendly style.
		SetWindowLongPtr(ghMainWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(ghMainWnd, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	}

	// Reset the device with the changes.
	onLostDevice();
	HR(gd3dDevice->Reset(&gd3dPP));
	onResetDevice();
}

bool isDeviceLost() {
	// Get the state of the graphics device.
	HRESULT hr = gd3dDevice->TestCooperativeLevel();

	// If the device is lost and cannot be reset yet then
	// sleep for a bit and we'll try again on the next 
	// message loop cycle.
	if (hr == D3DERR_DEVICELOST) {
		Sleep(20);
		return true;
	}
	// Driver error, exit.
	else if (hr == D3DERR_DRIVERINTERNALERROR) {
		MessageBox(0, _T("Internal Driver Error...Exiting"), 0, 0);
		PostQuitMessage(0);
		return true;
	}
	// The device is lost but we can reset and restore it.
	else if (hr == D3DERR_DEVICENOTRESET) {
		onLostDevice();
		HR(gd3dDevice->Reset(&gd3dPP));
		onResetDevice();
		return false;
	}
	else {
		return false;
	}
}

LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam) {
	static bool minOrMaxed = false;

	RECT clientRect = { 0, 0, 0, 0 };

	switch (msg) {
	case WM_ACTIVATE:
		// capture the mouse when window is activated
		if (LOWORD(wParam) == WA_ACTIVE) {
			SetCapture(ghMainWnd);
			gMouse->acquire();
		}

	case WM_SIZE:
		if (gd3dDevice) {
			gd3dPP.BackBufferWidth = LOWORD(lParam);
			gd3dPP.BackBufferHeight = HIWORD(lParam);

			if (wParam == SIZE_MINIMIZED) {
				gAppPaused = true;
				minOrMaxed = true;
			}
			else if (wParam == SIZE_MAXIMIZED) {
				gAppPaused = false;
				minOrMaxed = true;
				onLostDevice();
				HR(gd3dDevice->Reset(&gd3dPP));
				onResetDevice();
			}
			else if (wParam == SIZE_RESTORED) {
				gAppPaused = false;
				// Are we restoring from a mimimized or maximized state, 
				// and are in windowed mode?  Then reset device.
				if (minOrMaxed && gd3dPP.Windowed) {
					onLostDevice();
					HR(gd3dDevice->Reset(&gd3dPP));
					onResetDevice();
				}
				minOrMaxed = false;
			}
		}
		return 0;

		// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
		// Here we reset everything based on the new window dimensions.
	case WM_EXITSIZEMOVE:
		GetClientRect(ghMainWnd, &clientRect);
		gd3dPP.BackBufferWidth = clientRect.right;
		gd3dPP.BackBufferHeight = clientRect.bottom;
		onLostDevice();
		HR(gd3dDevice->Reset(&gd3dPP));
		onResetDevice();
		return 0;

	case WM_CLOSE:
		DestroyWindow(ghMainWnd);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_KEYDOWN:
		gKeyboard.HandleKeyDown(wParam);
		return 0;

	case WM_KEYUP:
		gKeyboard.HandleKeyUp(wParam);
		return 0;
	}

	return DefWindowProc(ghMainWnd, msg, wParam, lParam);
}

//______________________________________________________________________________
// WINDOWS CALLBACK
LRESULT CALLBACK
WinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	// Don't start processing messages until the application has been created.
	if (gAppCreated)
		return msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}

//______________________________________________________________________________
// WINMAIN
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
	PSTR cmdLine, int showCmd) {
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string winCaption = "My Little Program";
	D3DDEVTYPE devType = D3DDEVTYPE_HAL;
	DWORD requestedVP = D3DCREATE_HARDWARE_VERTEXPROCESSING;

	//_____________________________________________________________
	// 1. Initiate window
	WNDCLASS wc = {};
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WinProc;
	wc.hInstance = hInstance;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.lpszClassName = _T("LegoSceneClass");
	if (!RegisterClass(&wc)) {
		MessageBox(0, _T("RegisterClass Failed"), 0, 0);
		PostQuitMessage(0);
	}

	RECT WindowSize = { 0, 0, gClientWidth, gClientHeight };
	AdjustWindowRect(&WindowSize, WS_OVERLAPPEDWINDOW, false);
	ghMainWnd = CreateWindow(_T("LegoSceneClass"), _T("Lego Scene"),
		WS_OVERLAPPEDWINDOW, 100, 100, WindowSize.right, WindowSize.bottom,
		0, 0, hInstance, 0);
	if (!ghMainWnd) {
		MessageBox(0, _T("CreateWindow Failed"), 0, 0);
		PostQuitMessage(0);
	}
	ShowWindow(ghMainWnd, SW_SHOW);
	UpdateWindow(ghMainWnd);

	//_____________________________________________________________
	// 2. Initiate DirectX
	gd3d = Direct3DCreate9(D3D_SDK_VERSION);
	if (!gd3d) {
		MessageBox(0, _T("Direct3DCreate9 Failed"), 0, 0);
		PostQuitMessage(0);
	}
	// Verify hardware support in windowed and full screen modes.
	D3DDISPLAYMODE displayMode;
	gd3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode);
	HR(gd3d->CheckDeviceType(D3DADAPTER_DEFAULT, devType,
		displayMode.Format, displayMode.Format, true));
	HR(gd3d->CheckDeviceType(D3DADAPTER_DEFAULT, devType,
		D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false));
	// Verify support for requested vertex processing and pure device.
	D3DCAPS9 deviceCaps;
	HR(gd3d->GetDeviceCaps(D3DADAPTER_DEFAULT, devType, &deviceCaps));
	DWORD deviceBehaviorFlags = 0;
	if (deviceCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
		deviceBehaviorFlags |= requestedVP;
	}
	else {
		deviceBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}
	// If pure device and HW T&L supported
	if (deviceCaps.DevCaps & D3DDEVCAPS_PUREDEVICE &&
		deviceBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING) {
		deviceBehaviorFlags |= D3DCREATE_PUREDEVICE;
	}

#ifdef MULTISAMPLE
	// Automatically detect and use the highest supported multisampling level
	D3DMULTISAMPLE_TYPE msType;
	DWORD msQualityLevels;
	for (int i = 16; i >= 0; --i) {
		msType = (D3DMULTISAMPLE_TYPE)i;
		HRESULT res = gd3d->CheckDeviceMultiSampleType(
			D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_A8R8G8B8, true,
			msType, &msQualityLevels);
		if (res == D3D_OK) {
			break;
		}
	}
#endif

	// Create the device
	gd3dPP.BackBufferWidth = 0;
	gd3dPP.BackBufferHeight = 0;
	gd3dPP.BackBufferFormat = D3DFMT_A8R8G8B8;
	gd3dPP.BackBufferCount = 1;
#ifdef MULTISAMPLE
	gd3dPP.MultiSampleType = msType;
	gd3dPP.MultiSampleQuality = msQualityLevels - 1;
#else	
	gd3dPP.MultiSampleType = D3DMULTISAMPLE_NONE;
	gd3dPP.MultiSampleQuality = 0;
#endif
	gd3dPP.SwapEffect = D3DSWAPEFFECT_DISCARD;
	gd3dPP.hDeviceWindow = ghMainWnd;
	gd3dPP.Windowed = true;
	gd3dPP.EnableAutoDepthStencil = true;
	gd3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
	gd3dPP.Flags = 0;
	gd3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	gd3dPP.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
	HR(gd3d->CreateDevice(D3DADAPTER_DEFAULT, devType, ghMainWnd,
		deviceBehaviorFlags, &gd3dPP, &gd3dDevice));

	// Check device caps
	D3DCAPS9 caps;
	HR(gd3dDevice->GetDeviceCaps(&caps));
	if (caps.VertexShaderVersion < D3DVS_VERSION(2, 0)) {
		MessageBox(0, _T("Vertex Shader version 2.0 required"), 0, 0);
		PostQuitMessage(0);
	}
	if (caps.PixelShaderVersion < D3DPS_VERSION(2, 0)) {
		MessageBox(0, _T("Pixel Shader version 2.0 required"), 0, 0);
		PostQuitMessage(0);
	}

	// Triangle culling
	//gd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	//_____________________________________________________________
	// 3. Initialize DirectInput
	HR(DirectInput8Create(hInstance, DIRECTINPUT_VERSION,
		IID_IDirectInput8, (void**)&gdinput, 0));
	MouseController mouseController(gdinput,
		DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	gMouse = &mouseController;  // Set a global pointer to the mouse

	//_____________________________________________________________
	// 4. Initialize Application

	initializeApp();
	gAppCreated = true;

	// Initialisation Complete!
	//*************************************************************
	//_____________________________________________________________
	// Run
	MSG msg;
	msg.message = WM_NULL;
	__int64 cntsPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	float secsPerCnt = 1.0f / (float)cntsPerSec;
	__int64 prevTimeStamp = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTimeStamp);

	while (msg.message != WM_QUIT) {
		// If there are windows messages, process them...
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		// Otherwise, do animation/game stuff.
		else {
			// If the application is paused then free some CPU 
			// cycles to other applications and then continue on
			// to the next frame.
			if (gAppPaused) {
				Sleep(20);
				continue;
			}
			else if (!isDeviceLost()) {
				__int64 currTimeStamp = 0;
				// Get time between iterations
				QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
				float dt = (currTimeStamp - prevTimeStamp)*secsPerCnt;

				updateScene(dt);
				drawScene();

				// Prepare for next iteration
				prevTimeStamp = currTimeStamp;
			}
		}
	}

	// Finished running!
	//****************************************************************
	//________________________________________________________________
	// Begin shutdown procedures
	shutdownApp();
	ReleaseCOM(gd3d);
	ReleaseCOM(gd3dDevice);

	return msg.wParam;
}